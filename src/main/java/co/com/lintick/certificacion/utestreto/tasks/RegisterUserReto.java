package co.com.lintick.certificacion.utestreto.tasks;

import co.com.lintick.certificacion.utestreto.userinterfaces.*;
import com.ibm.icu.util.PersianCalendar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegisterUserReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Home.REGISTER),
                SendKeys.of("Andres").into(Personal.FIRSTNAME),
                SendKeys.of("Molina").into(Personal.LASTNAME),
                SendKeys.of("correo2@hotmail.com").into(Personal.EMAIL),
                SendKeys.of("July").into(Personal.MONTH),
                SendKeys.of("5").into(Personal.DAY),
                SendKeys.of("1991").into(Personal.YEAR),
                Click.on(Personal.NEXTLOCATION),
                Click.on(Location.BUTTONOCATION),
                Click.on(Devices.SISTEMA),
                Click.on(Devices.Windows),
                Click.on(Devices.VERSION),
                Click.on(Devices.XP),
                Click.on(Devices.LANGUAJE),
                Click.on(Devices.ENGLISH),
                Click.on(Devices.BUTTONNEXT),
                SendKeys.of("Qwerasdf1234*").into(Complete.PASSWORD),
                SendKeys.of("Qwerasdf1234*").into(Complete.CONFIRMPASSWORD),
                Click.on(Complete.CHECKONE),
                Click.on(Complete.CHECKTWO),
                Click.on(Complete.BUTTONCOMPLETE)
        );
    }
    public static RegisterUserReto makeInformation(){
        return instrumented(RegisterUserReto.class);
    }

}
